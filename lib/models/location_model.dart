class LocationModel{
  final String idLocation;
  final String nameLocation;
  final String imageLocation;
  final String areaLocation;
  final String typeLocation;
  final String rateLocation;
  final String priceLocation;
  final String spaceLocation;

  const LocationModel({
    this.idLocation,
    this.nameLocation,
    this.imageLocation,
    this.areaLocation,
    this.typeLocation,
    this.rateLocation,
    this.priceLocation,
    this.spaceLocation,
  });
}

List<LocationModel> locationList = [
  const LocationModel(
      idLocation: "1",
      nameLocation: "Softwarica",
      imageLocation: "",
      areaLocation: "Maitidevi,Kathmandu",
      typeLocation: "College Parking",
      rateLocation: "4.8",
      priceLocation: "Rp 5.000/day",
      spaceLocation: "60"),
  
  const LocationModel(
      idLocation: "2",
      nameLocation: "City Center",
      imageLocation: "",
      areaLocation: "Kamal Pokhari,  Kathmandu",
      typeLocation: "Parking Space",
      rateLocation: "4.8",
      priceLocation: "Rp 5.000/day",
      spaceLocation: "60"),
  const LocationModel(
      idLocation: "3",
      nameLocation: "Gyaneshwor Parking",
      imageLocation: "",
      areaLocation: "gyaneshwor, Kathmandu",
      typeLocation: "Parking Center",
      rateLocation: "4.6",
      priceLocation: "Rp 5.000/hour",
      spaceLocation: "50 "),
  
];
