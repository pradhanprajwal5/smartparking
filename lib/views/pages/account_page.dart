import 'package:flutter/material.dart';
import 'package:flutter_parking_project/views/widget/color_library.dart';

class AccountPage extends StatefulWidget {
  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  @override
  Widget build(BuildContext context) {

    Widget _compileWidget = new Scaffold(
      backgroundColor: Colors.grey,
      body: new SafeArea(
        child: new Stack(
          children: <Widget>[
            ClipPath(
              clipper: WaveClipper2(),
              child: Container(
                child: Center(
                  child: Column(
                    children: [
                      SizedBox(height: 25,),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        child: Icon(Icons.person_outline_rounded,size: 100,),
                      
                      ),
                      SizedBox(height: 20,),
                      Text("Aman Wagle",style: TextStyle(fontSize: 21),),
                      SizedBox(height: 20,),
                      Text("Mobile Number: "+"9867889858",),
                      SizedBox(height: 20,),
                      Text("Email: "+"amanwagle12@gmail.com",),
                      
                      
                    ],
                  ),
                ),
                width: double.infinity,
                height: 300,
                decoration: BoxDecoration(color: ColorLibrary.secondary),
              ),
            ),
          ],
        ),
      ),
    );

    return _compileWidget;
  }
}

class WaveClipper1 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0.0, size.height - 50);

    var firstEndPoint = Offset(size.width * 0.6, size.height );
    var firstControlPoint = Offset(size.width * 0.45, size.height - 60 );
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secondEndPoint = Offset(size.width, size.height - 20);
    var secondControlPoint = Offset(size.width * 0.84, size.height - 50);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

class WaveClipper2 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0.0, size.height - 40);

    var firstEndPoint = Offset(size.width/2 , size.height - 40);
    var firstControlPoint = Offset(size.width*0.1, size.height - 25);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secondEndPoint = Offset(size.width*0.7 , size.height - 20);
    var secondControlPoint = Offset(size.width * 0.7, size.height - 50);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);
        

    var thirdEndPoint = Offset(size.width, size.height - 20);
    var thirdControlPoint = Offset(size.width, size.height);
    path.quadraticBezierTo(thirdControlPoint.dx, thirdControlPoint.dy,
        thirdEndPoint.dx, thirdEndPoint.dy);

    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
